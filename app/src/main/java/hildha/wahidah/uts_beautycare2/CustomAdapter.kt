package hildha.wahidah.uts_beautycare2

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

class CustomAdapter(
    val context: Context,
    arrayList: ArrayList<HashMap<String, Any>>
) : BaseAdapter() {
    val F_TITLE = "file_title"
    val F_URL = "file_url"
    val list = arrayList
    var uri = Uri.EMPTY

    inner class ViewHolder() {
        var txfileTitle: TextView? = null
        var txfileUrl: TextView? = null
        var imv: ImageView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = ViewHolder()
        var view = convertView
        if (convertView == null) {

            var inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater
            view = inflater.inflate(R.layout.row_data_tips, null, true)

            holder.txfileTitle = view!!.findViewById(R.id.txfileTitle) as TextView
            holder.txfileUrl = view!!.findViewById(R.id.txfileUrl) as TextView
            holder.imv = view!!.findViewById(R.id.imv) as ImageView

            view.tag = holder

        } else {
            holder = view!!.tag as ViewHolder
        }

        uri = Uri.parse(list.get(position).get(F_URL).toString())

        holder.txfileTitle!!.setText(list.get(position).get(F_TITLE).toString())
        holder.txfileUrl!!.setText(uri.toString())
        holder.txfileUrl!!.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW).setData(
                Uri.parse(holder.txfileUrl!!.text.toString())
            )
            context.startActivity(intent)
        }

        return view!!
    }

    override fun getItem(position: Int): Any {
        return list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return list.size
    }
}