package hildha.wahidah.uts_beautycare2

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_tips.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class TipsActivity : AppCompatActivity() {

    lateinit var storage: StorageReference
    lateinit var db: CollectionReference
    lateinit var alFile: ArrayList<HashMap<String, Any>>
    lateinit var adapter: CustomAdapter
    lateinit var uri: Uri
    val F_ID = "id"
    val F_NAME = "file_name"
    val F_TITLE = "file_title"
    val F_TYPE = "file_type"
    val F_URL = "file_url"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tips)
        alFile = ArrayList()
        uri = Uri.EMPTY
    }

    override fun onStart() {
        super.onStart()

        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("tips")
        db.addSnapshotListener { querySnapshot, firebaseFireStoreException ->
            if (firebaseFireStoreException != null) {
                firebaseFireStoreException.message?.let { Log.e("Firestore : ", it) }
            }
            showData()
        }
    }

    fun showData() {
        db.get().addOnSuccessListener { result ->
            alFile.clear()
            for (doc in result) {
                val hm = HashMap<String, Any>()
                hm.put(F_ID, doc.get(F_ID).toString())
                hm.put(F_TYPE, doc.get(F_TYPE).toString())
                hm.put(F_NAME, doc.get(F_NAME).toString())
                hm.put(F_TITLE, doc.get(F_TITLE).toString())
                hm.put(F_URL, doc.get(F_URL).toString())
                alFile.add(hm)
            }
            adapter = CustomAdapter(this, alFile)
            lsV.adapter = adapter
        }
    }
}