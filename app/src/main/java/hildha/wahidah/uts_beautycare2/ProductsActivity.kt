package hildha.wahidah.uts_beautycare2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SimpleAdapter
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_products.*

class ProductsActivity : AppCompatActivity() {

    val COLLECTION = "beautycare"
    val F_ID = "id"
    val F_NAME = "nama"
    val F_FUNCTION = "fungsi"
    val F_PRICE = "harga"
    lateinit var db: FirebaseFirestore
    lateinit var allProduct: ArrayList<HashMap<String, Any>>
    lateinit var adapter: SimpleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_products)
        allProduct = ArrayList()
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) e.message?.let { Log.d("firestore", it) }
            showData()
        }
    }

    fun showData() {
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            allProduct.clear()
            for (doc in result) {
                val hm = HashMap<String, Any>()
                hm.set(F_ID, doc.get(F_ID).toString())
                hm.set(F_NAME, doc.get(F_NAME).toString())
                hm.set(F_FUNCTION, doc.get(F_FUNCTION).toString())
                hm.set(F_PRICE, doc.get(F_PRICE).toString())
                allProduct.add(hm)
            }
            adapter = SimpleAdapter(
                this, allProduct, R.layout.row_data_products,
                arrayOf(F_NAME, F_FUNCTION, F_PRICE),
                intArrayOf(R.id.txNama, R.id.txFungsi, R.id.txHarga)
            )
            lsData.adapter = adapter
        }
    }
}