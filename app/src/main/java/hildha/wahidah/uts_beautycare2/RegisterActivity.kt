package hildha.wahidah.uts_beautycare2

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_register.*


class RegisterActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        btnRegister.setOnClickListener(this)
        txLogin.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnRegister -> {
                var email = edEmail.text.toString()
                var password = edPassword.text.toString()

                if (email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(
                        this,
                        "Email or Password Can't be Empty ", Toast.LENGTH_SHORT
                    ).show()
                } else {
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Registration...")
                    progressDialog.show()

                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener {
                            progressDialog.hide()
                            if (!it.isSuccessful) return@addOnCompleteListener
                            Toast.makeText(this, "Registration Successfully", Toast.LENGTH_SHORT)
                                .show()
                            finish()
                        }
                        .addOnFailureListener {
                            progressDialog.hide()
                            Toast.makeText(this, "Registration Failed", Toast.LENGTH_SHORT).show()
                        }
                }
            }
            R.id.txLogin -> {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
        }
    }
}